
# Design / Branding Materials for Dark Crystal Web3

- [Design Brief](./design-brief.md)
- [Storing a secret flow diagram](./img/flow-diagram-storing-a-secret.pdf)
- [Recovery flow diagram](./img/flow-diagram-recover.pdf)
- [Branding strategy exercise](./img/branding-1.pdf)
- [Branding implementation](./img/branding-2.pdf)
